<?php

$plugin = array(
  'title' => t('Provide a Views filter handler with options limited by other fields'),
  'class' => 'ViewsFilterOptionLimitBehaviorHandler',
  'behavior type' => 'field',
);
